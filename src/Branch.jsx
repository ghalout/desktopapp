import React, { useState } from 'react';
import { Collapse, Button, CardBody, Card } from 'reactstrap';

const Branch = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <div>
      <Button color="primary" onClick={toggle} style={{ marginBottom: '1rem' }}>Create Branch</Button>
      <Collapse isOpen={isOpen}>
        <Card>
          <CardBody>
  <h2>Create branch for task {props.id}</h2>
          </CardBody>
        </Card>
      </Collapse>
    </div>
  );
}

export default Branch;