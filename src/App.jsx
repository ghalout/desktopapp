import React, { Component } from 'react';
import './App.css';
import TaskList from './app/task/showlist'
import CreateTeam from './app/task/createteam'
import headerlogo from './app/shared/images/logo.png'
import HomeIcon from './app/shared/images/home-icon.png'
import DashboardIcon from './app/shared/images/dashboard-icon.png'
import CalendarIcon from './app/shared/images/calendar-icon.png'
import ChatIcon from './app/shared/images/chat-icon.png'
import ClockIcon from './app/shared/images/clock-icon.png'
import UserIcon from './app/shared/images/user-icon.png'
import SettingIcon from './app/shared/images/setting-icon.png'
import magnifyingicon from './app/shared/images/magnifying-icon.png'
import bellicon from './app/shared/images/bell-icon.png'
import profileimg from './app/shared/images/profile-img.png'
import ellipsisicon from './app/shared/images/ellipsis-icon.png'
import addicon from './app/shared/images/add.png'
import onlineicon from './app/shared/images/online-icon.png'
import offlineicon from './app/shared/images/offline-icon.png'
import Todo from './app/task/todolist';
import ChatWidget from './app/chat/ChatWidget';

class App extends Component {

  render() {
    return (


      <div className="wrapper">



        <div className="sidebar-left">
          <div className="logo-container">
            <a href="#">
              <img src={headerlogo}></img>
            </a>
          </div>

          <div className="navigation">
            <ul>
              <li><a href="#" className="active-menu"><img src={HomeIcon} width="24" height="24" alt=""></img></a></li>
              <li><a href="#"><img src={DashboardIcon}></img></a></li>
              <li><a href="#"><img src={CalendarIcon}></img></a></li>
              <li><a href="#"><img src={ChatIcon}></img></a></li>
              <li><a href="#"><img src={ClockIcon}></img></a></li>
              <li><a href="#"><img src={UserIcon}></img></a></li>
              <li><a href="#"><img src={SettingIcon}></img></a></li>
              <li><CreateTeam /></li>
            </ul>

          </div>

          <div className="other-option">
            <a href="#"><img src={ellipsisicon} width="24" height="24"></img></a>
          </div>

        </div>

        <div className="header">

          <div className="page-main-title">
            All Task
        </div>

          <ul className="navbar-nav">
            <li className="nav-item">
              <a href="#" className="nav-link">
                <img src={magnifyingicon} width="24" height="24"></img>
              </a>
            </li>
            <li className="nav-item">
              <a href="#" className="nav-link">
                <img src={bellicon} width="24" height="24"></img>
                <span className="badge-counter">3+</span>
              </a>
            </li>
            <li className="nav-item">
              <a href="#" className="nav-link">
                <img src={profileimg} width="43" height="43"></img>
              </a>
            </li>
          </ul>

        </div>

        {/* <div className="chat-user-wrapper">
                  <ul className="user-list">
                    <li className="list-header">Direct messages
                      <img src={addicon} width="20" height="20" className="add-user-icon" alt="Open a direct message" title="Open a direct message"></img>
                    </li>
                    <li className="user-separator"></li>
                    <li className="user-item">
                    <img src={onlineicon} width="12" height="12" className="status-icon" alt="Online Icon"></img>
                      Jitender Kumar</li>
                    <li className="user-item">
                    <img src={onlineicon} width="12" height="12" className="status-icon" alt="Online Icon"></img>
                      Bheem Singh</li>
                    <li className="user-item">
                    <img src={offlineicon} width="12" height="12" className="status-icon" alt="Online Icon"></img>
                      Raman Kumar</li>
                    <li className="user-item">
                    <img src={offlineicon} width="12" height="12" className="status-icon" alt="Online Icon"></img>
                      Prateek Gupta</li>

                    <li className="list-header list-team-header">Team
                      <img src={addicon} width="20" height="20" className="add-user-icon" alt="Create a Team" title="Create a Team"></img>
                    </li>
                    <li className="user-separator"></li>
                    <li className="user-item">@ Hackathon-Delhi</li>
                    <li className="user-item">@ FunCom</li>
                    <li className="user-item">@ Team-Delhi</li>
                    <li className="user-item">@ PaybotUs Group</li>
                  </ul>
                </div> */}

        <div className="content-body">

          {/* <div className="row">
            <div className="col-md-12">
              <div className="chat-widget-wrapper">
                <ChatWidget></ChatWidget>
              </div>
            </div>
          </div> */}

          {/* <div className="App-header">
            <CreateTeam />
          </div> */}

          <div className="row">
            <div className="col-md-6">
              <div className="task-panel">
                <TaskList />
              </div>
            </div>
            <div className="col-md-6">
              <div className="task-panel">
                <Todo />
              </div>
            </div>
          </div>

        </div>


      </div>
    );
  }
}

export default App;
