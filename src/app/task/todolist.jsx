import React, { useState } from 'react';
import './todolist.css';
import { TabContent, TabPane, Nav, NavItem, NavLink, Row, Col } from 'reactstrap';
import classnames from 'classnames';
import ellipsisicon2 from '../../app/shared/images/ellipsis-icon2.png'

export default class Todo extends React.Component {
    taskList = [
        {

            value: 'fill time sheet',

        }, {

            value: 'create strory on jira',

        }
    ]
    constructor(props) {

        super(props);
      
        this.toggle = this.toggle.bind(this);
        this.state = {
            activeTab: '1',
            value:'',
            isLoading: true,
            list: ['fill time sheet', 'create strory on jira', 'intent training'],
        };
    }

    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({ activeTab: tab });
        }
    }
    onChangeValue = event => {
        this.setState({ value: event.target.value });
      };
      onAddItem = () => {
        this.setState(state => {
          const list = state.list.concat(state.value);
          return {
            list,
            value: '',
          };
        });
      };
    render() {
        return (
            <div className="task-wrapper">
                <Nav tabs>
                    <NavItem>
                        <NavLink
                            className={classnames({ active: this.state.activeTab === '1' })}
                            onClick={() => { this.toggle('1'); }}
                        >
                            ToDo
          </NavLink>
                    </NavItem>

                    <NavItem>
                        <NavLink
                            className={classnames({ active: this.state.activeTab === '2' })}
                            onClick={() => { this.toggle('2'); }}
                        >
                            Activity Feed
          </NavLink>
                    </NavItem>

                    {/* <NavItem>
                    <NavLink
                        className={classnames({ active: activeTab === '3' })}
                        onClick={() => { toggle('3'); }}
                    >
                        Activity Feed
          </NavLink>
                </NavItem> */}



                </Nav>

                <div className="task-list-wrapper">

                    <TabContent activeTab={this.state.activeTab}>

                        <TabPane tabId="1">

                            <ul className="todo-list">
                                {this.state.list.map((item, index) =>
                                    <li>
                                        <div className="todo-checkbox">
                                            <label for="checkbox">
                                                <input type="checkbox" name="checkbox" value="checkbox"></input>
                                            </label>
                                        </div>
                                        <div className="todo-title">
                                            <a href="#">{item}</a>
                                        </div>
                                        <div className="todo-action">
                                            <img src={ellipsisicon2} width="19" height="19" alt=""></img>
                                        </div>
                                    </li>
                                )}

                            </ul>
                            <div className="add-todo-wrapper">
                            
                            <input  type="text"
          value={this.state.value}
          onChange={this.onChangeValue} name="" id="" placeholder="Add todo..."></input>
                            <button type="button"
          onClick={this.onAddItem}
          disabled={!this.state.value} name="" value="" id="" className="add-todo-btn btn btn-primary">Add</button>
                        </div>
                            
                                {/* <AvForm onValidSubmit={this.handleValidSubmit} >
                                <div className="add-todo-wrapper">
                                   
                                    <AvField   type="text" name="todDo" placeholder="Add todo..." id="todo" required />
                                    <Button color="primary" className="add-todo-btn btn btn-primary">Add</Button>
                                    </div>
                                </AvForm> */}
                            
                        </TabPane>
                        {/* <TabPane tabId="1">
                        <div className="coming-soon">Coming Soon!</div>
                    </TabPane> */}
                        <TabPane tabId="2">
                            <div className="coming-soon">Coming Soon!</div>
                        </TabPane>

                        {/* <TabPane tabId="3">
                        <ul className="task-list">
                            {(state.taskList || []).map(item => (
                                <li key={item.id} className="task">
                                    <div className="task-poritiy"></div>
                                    <div className="project-profile-img">
                                        <img src={projectprofileimg} width="43" height="43" alt=""></img>
                                    </div>
                                    <div className="task-company-name">{item.value}<span>In <a href="#">Sports Interactive</a></span>
                                    </div>
                                    <div className="task-assigne">
                                        <img src={profileimg2} width="38" height="37" alt="Donya Pour" title="Donya Pour"></img>
                                        <img src={profileimg} width="38" height="37" alt="Jitender Kumar" title="Jitender Kumar"></img>
                                    </div>
                                    <div className="task-reminder">
                                        <img src={bellicon} width="16" height="16" alt="Bell Icon" title="Donya Pour"></img> Oct,26
                                    </div>
                                    <div className="other-action">
                                        <img src={ellipsisicon2} width="19" height="19" alt="Ellipsis Icon"></img>
                                    </div>
                                    <div className="display-none">{item.id}</div>
                                    <div className="display-none">{item.assigne}</div>
                                </li>
                            ))}
                        </ul>
                    </TabPane> */}

                    </TabContent>
                </div>

            </div>
        );
    }
}

