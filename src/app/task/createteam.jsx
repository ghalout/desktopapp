import React, { useState } from 'react';
import './createteam.css';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Col, Form, FormGroup, Label, Input } from 'reactstrap';
import { AvForm, AvField, AvGroup, AvInput, AvFeedback, AvRadioGroup, AvRadio, AvCheckboxGroup, AvCheckbox } from 'availity-reactstrap-validation';
import Select from 'react-select';
import groupicon from '../../app/shared/images/users-group.png'
import usersgroupicon from '../../app/shared/images/users-group2.png'
import path from '../shared/path';

const options = [

];
export default class CreateTeam extends React.Component {
  state = {
    selectedOption: null,
    showModal: false
  };
  emails = []
  data = {}
  constructor(props) {
    super(props);

    this.handleValidSubmit = this.handleValidSubmit.bind(this);
    this.toggleModal = this.toggleModal.bind(this);
  }

  componentDidMount() {
    fetch(path.allUserApi)
      .then(res => res.json())
      .then((result) => {
        let resp = result.data;
        for (let i = 0; i < resp.length; i++) {
          options.push({ value: resp[i].userLoginId, label: resp[i].userName })
        }
      })

  }

  handleChange = selectedOption => {
    this.setState(
      { selectedOption },
      () => console.log(`Option selected:`, this.state.selectedOption)
    );
  };
  handleValidSubmit(event, values) {
    
    console.log(values)
    this.data = {
      'name': values.team,
      'completeName': values.team,
      'userId': 'amit.tanwar@paxcel.net',
      'companyId': 1,
      'companyName': 'Paxcel'
    }
    var request = new Request('http://localhost:2020/api/leave/getappliedleavetype', {
      method: 'POST',
      headers: new Headers({ 'Content-Type': 'application/json' }),
      body: JSON.stringify({url:'http://3.210.82.109/DrutasWebAPI/Project/AddProject',data:this.data})
    });
    fetch(request)
      .then(response => response.json())
      .then((result) => {
        console.log(result.data)
        this.addMemeber(JSON.parse(result.data).data.resourceId,this.state.selectedOption)
      })
    this.toggleModal()
    this.setState({ values });
  }
  addMemeber(groupId,memberlist){
    this.emails = []
    for (let i = 0; i < memberlist.length; i++) {
      this.emails.push( memberlist[i].value)
    }
    this.data = {
      'usersToShareArray': this.emails,
      'usersToUnshareArray':[],
      'groupId':groupId,
      'companyId': 1
    }
    var request = new Request('http://localhost:2020/api/leave/putappliedleavetype', {
      method: 'PUT',
      headers: new Headers({ 'Content-Type': 'application/json' }),
      body: JSON.stringify({url:'http://3.210.82.109/DrutasWebAPI/Project/UpdateProjectUsers',data:this.data})
    });
    fetch(request)
      .then(response => response.json())
      .then((result) => {
        console.log(result.data)
        
      })
  }
  toggleModal() {

    this.setState({
      showModal: !this.state.showModal
    })

  }
  render() {
    const { selectedOption } = this.state;
    return (
      <div>
        <a href="#"><img src={groupicon} onClick={this.toggleModal} width="24" height="24" alt="Team Icon" title="Create New Team"></img></a>
        {/* <Button color="danger" onClick={this.toggleModal}>Create Team</Button> */}

        <Modal isOpen={this.state.showModal} toggle={this.toggleModal} className="create-team-wrapper">

          <ModalHeader toggle={this.toggleModal}><img src={usersgroupicon} width="22" height="22" alt="Team Icon"></img> Create New Team</ModalHeader>

          <ModalBody>

            <AvForm onValidSubmit={this.handleValidSubmit}>

              <AvField name="team" placeholder="Team or project name" required />

              <AvGroup>
                {/* <Label for="members" sm={2}>Members</Label> */}
                <Select
                  value={selectedOption}
                  onChange={this.handleChange}
                  options={options}
                  placeholder="Memebr add"
                  isMulti />
              </AvGroup>

              <AvGroup>
                {/* <Label for="members">Team Description</Label> */}
                <AvInput type="textarea" name="description" id="description" placeholder="Team Description" required />
                <AvFeedback>This is an error!</AvFeedback>

              </AvGroup>

              <FormGroup>
                <Button color="primary" className="add-btn">Create Team</Button>
              </FormGroup>

            </AvForm>

          </ModalBody>

        </Modal>

      </div>

    );
  }
}