import React, { useState } from 'react';
import './showlist.css';
import { TabContent, TabPane, Nav, NavItem, NavLink, Row, Col } from 'reactstrap';
import classnames from 'classnames';
import task from '../shared/shared'
import AddTask from './addtask'
import projectprofileimg from '../../app/shared/images/project-profile-img.png'
import profileimg from '../../app/shared/images/profile-img.png'
import profileimg2 from '../../app/shared/images/profile-img2.png'
import bellicon from '../../app/shared/images/bell-icon.png'
import ellipsisicon2 from '../../app/shared/images/ellipsis-icon2.png'
import Spinner from 'react-bootstrap/Spinner'
class TaskList extends React.Component {
    data = {
        'currentStatusIds':[1,2,3,4,5,6,7,8,9,10,11],
        'eleaseIds': [6167],
        'taskTypeIds': [1],
        'taskTypeIds': [2],
        'taskTypeIds': [3],
        'groupId': 1752
    }
    constructor(props) {

        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            activeTab: '1',
            taskList: [],
            isLoading: true,
        };
    }



    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({ activeTab: tab });
        }
    }
    inprogress() {
        this.data = {}
        this.data = {
            'currentStatusIds': [3],
            'releaseIds': [6167],
            'taskTypeIds': [3,1,2],
            'groupId': 1752
        }
        this.apiCall(this.data);
    }
    completeTask() {
        this.data = {}
        this.data = {
            'currentStatusIds': [4],
            'eleaseIds': [6167],
            'taskTypeIds': [3,1,2],
            'groupId': 1752
        }

        this.apiCall(this.data);
    }
    all() {
        this.data = {}
        this.data = {
            'currentStatusIds':[1,2,3,4,5,6,7,8,9,10,11],
            'eleaseIds': [6167],
            'taskTypeIds': [1],
            'taskTypeIds': [2],
            'taskTypeIds': [3],
            'groupId': 1752
        }
        this.apiCall(this.data);
    }
    componentDidMount() {

        this.apiCall(this.data);
    }

    apiCall(data) {
        this.state.taskList = [];
        this.state.isLoading = true;
        var request = new Request('http://localhost:2020/api/leave/getappliedleavetype', {
            method: 'POST',
            headers: new Headers({ 'Content-Type': 'application/json' }),
            body: JSON.stringify({ url: 'http://3.210.82.109/DrutasWebAPI/task/getTasksByFilterMap', data: data })
        });
        fetch(request)
            .then(response => response.json())
            .then((result) => {
                const data = JSON.parse(result.data).data;
                console.log(data)
                this.setState({ taskList: data.slice(0, 200) ,isLoading: false})
            })
    }
   

    //  allTask()
    render() {
        return (
            <div className="task-wrapper">
                <Nav tabs>
                    <NavItem>
                        <NavLink
                            className={classnames({ active: this.state.activeTab === '1' })}
                            onClick={() => { this.toggle('1'); this.all(); }}
                        >
                            All
          </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink
                            className={classnames({ active: this.state.activeTab === '2' })}
                            onClick={() => { this.toggle('2'); this.inprogress() }}
                        >
                            In Progress
          </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink
                            className={classnames({ active: this.state.activeTab === '3' })}
                            onClick={() => { this.toggle('3'); this.completeTask(); }}
                        >
                            Completed Task
          </NavLink>
                    </NavItem>

                    <AddTask />

                </Nav>

                <div className="task-list-wrapper">

                { this.state.isLoading &&
  <Spinner animation="border" />
}
                    <TabContent activeTab="1">

                        <TabPane tabId="1">
                            <ul className="task-list">
                                {this.state.taskList.map((item, index) =>
                                    (<li key={item.name} className="task">
                                        <div className="task-poritiy"></div>
                                        <div className="project-profile-img">
                                            A
                                        {/* <img src={projectprofileimg} width="43" height="43" alt=""></img> */}
                                        </div>
                                        <div className="task-company-name">{item.name}<span>In <a href="#">{item.assignedBy.userLoginId}</a></span>
                                        </div>
                                        <div className="task-assigne">
                                            <img src={profileimg2} width="38" height="37" alt="Donya Pour" title="Donya Pour"></img>
                                            <img src={profileimg} width="38" height="37" alt="Jitender Kumar" title="Jitender Kumar"></img>
                                        </div>
                                        <div className="task-reminder">
                                            <img src={bellicon} width="16" height="16" alt="Bell Icon" title="Donya Pour"></img> Oct, 26
                                </div>
                                        <div className="other-action">
                                            <img src={ellipsisicon2} width="19" height="19" alt="Ellipsis Icon"></img>
                                        </div>

                                        <div className="display-none">{item.assignedTo}</div>
                                    </li>)
                                )}
                            </ul>
                        </TabPane>

                        <TabPane tabId="2">
                        <ul className="task-list">
                                {this.state.taskList.map((item, index) =>
                                    (<li key={item.name} className="task">
                                        <div className="task-poritiy"></div>
                                        <div className="project-profile-img">
                                            A
                                        {/* <img src={projectprofileimg} width="43" height="43" alt=""></img> */}
                                        </div>
                                        <div className="task-company-name">{item.name}<span>In <a href="#">{item.assignedBy.userLoginId}</a></span>
                                        </div>
                                        <div className="task-assigne">
                                            <img src={profileimg2} width="38" height="37" alt="Donya Pour" title="Donya Pour"></img>
                                            <img src={profileimg} width="38" height="37" alt="Jitender Kumar" title="Jitender Kumar"></img>
                                        </div>
                                        <div className="task-reminder">
                                            <img src={bellicon} width="16" height="16" alt="Bell Icon" title="Donya Pour"></img> Oct, 26
                                </div>
                                        <div className="other-action">
                                            <img src={ellipsisicon2} width="19" height="19" alt="Ellipsis Icon"></img>
                                        </div>

                                        <div className="display-none">{item.assignedTo}</div>
                                    </li>)
                                )}
                            </ul>
                        </TabPane>

                        <TabPane tabId="3">
                            <ul className="task-list">
                                {this.state.taskList.map((item, index) =>
                                    (<li key={item.name} className="task">
                                        <div className="task-poritiy"></div>
                                        <div className="project-profile-img">
                                            A
                                        {/* <img src={projectprofileimg} width="43" height="43" alt=""></img> */}
                                        </div>
                                        <div className="task-company-name">{item.name}<span>In <a href="#">{item.assignedBy.userLoginId}</a></span>
                                        </div>
                                        <div className="task-assigne">
                                            <img src={profileimg2} width="38" height="37" alt="Donya Pour" title="Donya Pour"></img>
                                            <img src={profileimg} width="38" height="37" alt="Jitender Kumar" title="Jitender Kumar"></img>
                                        </div>
                                        <div className="task-reminder">
                                            <img src={bellicon} width="16" height="16" alt="Bell Icon" title="Donya Pour"></img> Oct, 26
                                </div>
                                        <div className="other-action">
                                            <img src={ellipsisicon2} width="19" height="19" alt="Ellipsis Icon"></img>
                                        </div>

                                        <div className="display-none">{item.assignedTo}</div>
                                    </li>)
                                )}
                            </ul>
                        </TabPane>

                    </TabContent>
                </div>
            </div>
        );
    }
}

export default TaskList;