import React, { useState } from 'react';
import './addtask.css';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, FormGroup, Label } from 'reactstrap';
import task from '../shared/shared'
import taskaddicon from '../../app/shared/images/add.png'
import filtericon from '../../app/shared/images/funnel.png'
import clipboard from '../../app/shared/images/clipboard.png'
import path from '../shared/path';
import Select from 'react-select';
import { AvForm, AvField, AvGroup, AvInput, AvFeedback, AvRadioGroup, AvRadio, AvCheckboxGroup, AvCheckbox } from 'availity-reactstrap-validation';
const options = [

];
class AddTask extends React.Component {
    state = {
        selectedOption: null,
        showModal: false,
        taskType: task.taskTypeList,
        statusType: task.taskPriority
    };
    data = {}
    constructor(props) {
        super(props);

        this.handleValidSubmit = this.handleValidSubmit.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
    }
    handleChange = selectedOption => {
        this.setState(
          { selectedOption },
          () => console.log(`Option selected:`, this.state.selectedOption.value)
        );
      };
    handleValidSubmit(event, values) {
        this.data = {
            'assignedTo': { 'userLoginId': this.state.selectedOption.value },
            'assignedBy': { 'userLoginId': 'amit.tanwar@paxcel.net' },
            'author': { 'userLoginId': 'amit.tanwar@paxcel.net' },
            'name': values.task,
            'estimatedNumberOfMinutes': 61,
            'isAccepted': true,
            'currentStatus': 'New',
            'currentStatusId': 1,
            'dueDate': '',
            'plannedDate': '',
            'versionNumber': '',
            'versionNumber': 0,
            'taskGroup': 'Default',
            'taskType': 'Task',
            'taskTypeId': 1,
            'taskUpdatedDate':'' ,
            'releasePriority': '',
            'releasePriorityId': 5,
            'useCaseId': '',
            'groupId': 1752,
            'updatedBy': { 'userLoginId': 'amit.tanwar@paxcel.net' },
            'ignoreVersionNumberCheck': true

        }
        var request = new Request('http://localhost:2020/api/leave/getappliedleavetype', {
            method: 'POST',
            headers: new Headers({ 'Content-Type': 'application/json' }),
            body: JSON.stringify({ url: 'http://3.210.82.109/DrutasWebAPI/task/createTask', data: this.data })
        });
        fetch(request)
            .then(response => response.json())
            .then((result) => {
                console.log(result.data)
               
            })
        this.toggleModal()
        this.setState({ values });
    }
    componentDidMount() {
        fetch(path.allUserApi)
            .then(res => res.json())
            .then((result) => {
                let resp = result.data;
                for (let i = 0; i < resp.length; i++) {
                    options.push({ value: resp[i].userLoginId, label: resp[i].userName })
                }
            })

    }
    toggleModal() {

        this.setState({
            showModal: !this.state.showModal
        })
    }

    render() {
        const { selectedOption } = this.state;
        return (
            <div>
                <div className="task-action">
                    <img src={taskaddicon} width="20" height="20" alt="Add New Task" title="Add New Task" onClick={this.toggleModal}></img>
                    <img src={filtericon} width="20" height="20" alt="Apply Filter" title="Apply Filter"></img>
                </div>

                {/* <Button color="danger" onClick={toggle}>Add Task</Button> */}

                <Modal isOpen={this.state.showModal} toggle={this.toggleModal} className="add-task-wrapper">

                    <ModalHeader toggle={this.toggleModal}><img src={clipboard} width="22" height="22" alt="Clipboard Icon"></img> Add New Task</ModalHeader>

                    <ModalBody>
                        {/* <form>

                        <div class="form-group">
                            <input type="text" placeholder="Task Title" class="form-control" id="task"></input>
                        </div>

                        <div class="form-group">
                            <input type="text" placeholder="Assigne" class="form-control" id="Assign"></input>
                        </div>
                        <div class="form-group">
                            <div className="row">
                            <div className="col-md-3">
                                <input type="text" placeholder="Start Date" class="form-control" id="StartDate"></input>
                            </div>
                            <div className="col-md-3">
                                <input type="text" placeholder="Due Date" class="form-control" id="DueDate"></input>
                            </div>
                            <div className="col-md-3">
                                <input type="text" placeholder="Due Time" class="form-control" id="DueTime"></input>
                            </div>
                            <div className="col-md-3">
                                <input type="text" placeholder="Days" class="form-control" id="Days"></input>
                            </div>
                            </div>
                        </div>
                        <div class="form-group">
                           <div className="row">
                           <div className="col-md-6">
                                <select id="TaskType" className="form-control">
                                    {this.state.taskType.map((item) => <option key={item.id} value={item.id}>{item.value}</option>)}
                                </select>
                            </div>
                            <div className="col-md-6">
                                <select id="Status" className="form-control">
                                    {this.state.statusType.map((item) => <option key={item.id} value={item.id}>{item.value}</option>)}
                                </select>
                            </div>
                           </div>
                        </div>
                        <div class="form-group">
                            <textarea type="text" placeholder="Description" class="form-control" id="Description"></textarea>
                        </div>

                    </form> */}
                        <AvForm onValidSubmit={this.handleValidSubmit}>

                            <AvField name="task" placeholder="Task Title" required />


{/* 
                            <AvGroup>
                                <AvField name="assigne" placeholder="Assigne" required />
                                <AvFeedback>This is an error!</AvFeedback>

                            </AvGroup> */}
                                    <AvGroup>
                {/* <Label for="members" sm={2}>Members</Label> */}
                <Select
                  value={selectedOption}
                  onChange={this.handleChange}
                  options={options}
                  placeholder="Assigne"
                   />
              </AvGroup>

                            <AvGroup className="task-row-margin">
                                <div className="row">
                                    <div className="col-md-6">
                                        <AvField name="start" placeholder="Start Date" required />
                                    </div>
                                    <div className="col-md-6">
                                        <AvField name="end" placeholder="End Date" required />
                                    </div>
                                    <AvFeedback>This is an error!</AvFeedback>
                                </div>
                            </AvGroup>

                            <AvGroup className="task-row-margin">
                                <div className="row">
                                    <div className="col-md-6">
                                        <AvField name="due" placeholder="Due Time" required />
                                    </div>
                                    <div className="col-md-6">
                                        <AvField name="days" placeholder="Days" required />
                                    </div>
                                    <AvFeedback>This is an error!</AvFeedback>
                                </div>
                            </AvGroup>

                            <div className="row">
                                <div className="col-md-6">
                                    <AvField type="select" name="taskType">
                                        <option>Task</option>
                                        <option>Bug</option>
                                        <option>Feature</option>
                                    </AvField>
                                </div>
                                <div className="col-md-6">
                                    <AvField type="select" name="taskpriority">
                                        <option>Low</option>
                                        <option>High</option>
                                        <option>Critical</option>
                                    </AvField>
                                </div>
                            </div>

                            <AvGroup>
                                {/* <Label for="members">Team Description</Label> */}
                                <AvInput type="textarea" name="description" id="description" placeholder="Description" required />
                                <AvFeedback>This is an error!</AvFeedback>

                            </AvGroup>

                            <FormGroup>
                                <Button color="primary" className="add-btn">Add Task</Button>
                            </FormGroup>

                        </AvForm>
                    </ModalBody>


                </Modal>

            </div>
        );
    }
}
export default AddTask;