const sgMail = require('@sendgrid/mail');
sgMail.setApiKey("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");

const mailOptions = {
to: 'amittanwar@mailinator.com',
from: {
email: "drutas@paxcel.net",
name: "Drutas"
},
subject: 'Sending with SendGrid is Fun',
html: '<strong>and easy to do anywhere, even with Node.js</strong>',
};

sendMail = function (data) {
mailOptions.to = data.email;
if (data.subject) {
mailOptions.subject = data.subject;//'Welcome Email From Learning With Vodafone';
}
if (data.html) {
mailOptions.html = data.html;
}

if (typeof mailOptions.to === "string") {
sgMail.send(mailOptions).then(sent => {
// Awesome Logic to check if mail was sent
//console.log('successful')
}).catch(err => logger.error(`error email: ${err}`))
} else {
sgMail.sendMultiple(mailOptions, (err, suc) => {
if (err) {
logger.error(`error email: ${err}`);
} else {
//console.log('successful')
return "";
}
})
}
}

function generateEmailReminderHTML(data, pmUrl = "http://app.drutas.com/pm", url = "http://app.drutas.com") {
var strVar = "";
strVar += "<!DOCTYPE html PUBLIC \"-\/\/W3C\/\/DTD XHTML 1.0 Transitional\/\/EN\" \"http:\/\/www.w3.org\/TR\/xhtml1\/DTD\/xhtml1-transitional.dtd\">";
strVar += "<html xmlns=\"http:\/\/www.w3.org\/1999\/xhtml\">";
strVar += "<head>";
strVar += "<meta http-equiv=\"Content-Type\" content=\"text\/html; charset=utf-8\" \/>";
strVar += "<title>Untitled Document<\/title>";
strVar += "";
strVar += "<style>";
strVar += "body{";
strVar += " margin:0;";
strVar += " padding:0;";
strVar += " font-size:12px;";
strVar += " color:#333;";
strVar += " font-family:Arial, Helvetica, sans-serif; ";
strVar += "}";
strVar += "<\/style>";
strVar += "<\/head>";
strVar += "<body>";
var header = '<div style="width:700px; height:55px; background:#c3c4c5; overflow:hidden; margin:0 auto; border: 1px solid #cccccc; font-family:Arial, Helvetica, sans-serif; font-size:12px;">' +
'<div>' +
'<div style="float:left">' +
// '<a href="http://app.drutas.com/qa/pm/index.html" target="_blank"><img src="http://app.drutas.com/images_for_email/drutaslogo.png" style="padding: 5px;"></a>' +
'<a href="' + pmUrl + '" target="_blank"><img src="http://app.drutas.com/images_for_email/drutaslogo.png" style="padding: 5px;"></a>' +
'</div>' +
'<div style="float:right; margin:10px 0; overflow:hidden;">' +
'<span style="padding: 0 5px;"><a href="https://www.facebook.com/drutas.paxcel" target="_blank"><img src="http://app.drutas.com/images_for_email/facebook.png" /></a></span style="padding: 0 5px;"> ' +
'<span style="padding: 0 5px;"><a href="https://twitter.com/Drutas" target="_blank"><img src="http://app.drutas.com/images_for_email/twitter.png" /></a></span style="padding: 0 5px;"> ' +
'<span style="padding: 0 5px;"><a href="https://plus.google.com/+PaxcelNetdrutas" target="_blank"><img src="http://app.drutas.com/images_for_email/google_plus.png" /></a></span style="padding: 0 5px;">' +
'</div>' +
'</div>' +
'</div>';
var content = '<div style="width:700px; margin:0 auto; border: 1px solid #cccccc; padding-bottom:20px; font-family:Arial, Helvetica, sans-serif; font-size:12px;">' +
'<div style="font-size:12px; font-family:Arial, Helvetica, sans-serif;">' +
'<p style="padding-left:10px; line-height:5px">' + data + '</p>' +

// '<ul style="width: 550px;">' + unescape(encodeURIComponent(details)) + '</ul>' +
'</div>' +
'</div>';
var footer = '<div style="width:700px; margin:0 auto; border: 1px solid #cccccc; margin-top:-1px; font-family:Arial, Helvetica, sans-serif; font-size:12px;">' +
'<p style="padding:10px"><i>This is an auto generated mail sent via Drutas. <br>Please do not reply.</i>';
footer = footer + '<a href="' + url + '" style="float:right;background-color:#659c1c;border-radius:2px;color:#fff;display:inline-block;font-size:14px;font-weight:600;line-height:30px;text-align:center;text-decoration:none;width:100px">Go to Drutas</a></p></div>';
var final = strVar + header + content + footer + "</body></html>";
return final;
}


export default { sendMail, generateEmailReminderHTML }

