const typeList = {
    taskTypeList: [
        {
            id: 1,
            value: 'Task'
        }, {
            id: 2,
            value: 'Bug'
        }, {
            id: 3,
            value: 'Feature'
        }
    ],
    taskPriority: [
        {
            id: 1,
            value: 'Low'
        }, {
            id: 2,
            value: 'High'
        }, {
            id: 3,
            value: 'Critical'
        }
    ],
    taskList:[
        {
            id: 1,
            value: ' task one',
            assigne:'bheem '
        }, {
            id: 2,
            value: ' task two',
            assigne:'bheem '
        }, {
            id: 3,
            value: ' task three',
            assigne:'bheem '
        }
    ]
}

export default typeList
