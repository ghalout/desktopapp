import React, { Component } from 'react';
import "./chatwidget.css";
import profileimg from '../../app/shared/images/profile-img.png'
import profileimg2 from '../../app/shared/images/profile-img2.png'
import projectprofile from '../../app/shared/images/project-profile-img.png'
import smileyfaceicon from '../../app/shared/images/smiley-face.png'
import attachicon from '../../app/shared/images/attach.png'
import ellipsisicon from '../../app/shared/images/ellipsis-icon2.png'
const style1 = {
    color: "rgb(48, 92, 143)",
    backgroundColor: "rgb(220, 227, 237)"
}
class ChatWidget extends Component {
    constructor(props) {
        super(props)

        this.state = {

        }
    }

    render() {
        return (
            <div>
                <div className="chat-header">
                    Group or user name
            </div>

                <ul className="todo-list-inner">
                    <li className="chat-day">
                        <span>Today</span>
                    </li>
                    <li class="chat-list">
                        <div class="profile-img">
                            <img src={profileimg} width="43" height="43" alt="Profile Image"></img>
                        </div>
                        <div class="chat-user-name">Jitender Kumar <span className="chat-post-time">Sat 02:05 AM</span>
                            <span className="chat-post-text"><a href="#">@Donyapour</a> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</span>
                        </div>
                    </li>
                    <li class="chat-list">
                        <div class="profile-img">
                            <img src={profileimg2} width="43" height="43" alt="Profile Image"></img>
                        </div>
                        <div class="chat-user-name">Donya Pour <span className="chat-post-time">Sat 02:05 AM</span>
                            <span className="chat-post-text">It is a long established fact that a reader will be distracted by <a href="#">@Bheemsingh</a> the readable content of a page when looking at its layout.</span>
                        </div>
                    </li>
                    <li class="chat-list">
                        <div class="profile-img">
                            <img src={projectprofile} width="43" height="43" alt="Profile Image"></img>
                        </div>
                        <div class="chat-user-name">Sushil Kumar <span className="chat-post-time">Sat 02:05 AM</span>
                            <span className="chat-post-text">The generated Lorem Ipsum is therefore always free from repetition, injected humour, <a href="#">https://paxcom.net/</a> or non-characteristic words etc. Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</span>
                        </div>
                    </li>
                    <li class="chat-list">
                        <div class="profile-img">
                            <img src={profileimg} width="43" height="43" alt="Profile Image"></img>
                        </div>
                        <div class="chat-user-name">Jitender Kumar <span className="chat-post-time">Sat 02:05 AM</span>
                            <span className="chat-post-text"><a href="#">@Donyapour</a> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</span>
                        </div>
                    </li>
                    <li class="chat-list">
                        <div class="profile-img">
                            <img src={profileimg2} width="43" height="43" alt="Profile Image"></img>
                        </div>
                        <div class="chat-user-name">Donya Pour <span className="chat-post-time">Sat 02:05 AM</span>
                            <span className="chat-post-text">It is a long established fact that a reader will be distracted by <a href="#">@Bheemsingh</a> the readable content of a page when looking at its layout.</span>
                        </div>
                    </li>
                    <li class="chat-list">
                        <div class="profile-img">
                            <img src={projectprofile} width="43" height="43" alt="Profile Image"></img>
                        </div>
                        <div class="chat-user-name">Sushil Kumar <span className="chat-post-time">Sat 02:05 AM</span>
                            <span className="chat-post-text">The generated Lorem Ipsum is therefore always free from repetition, injected humour, <a href="#">https://paxcom.net/</a> or non-characteristic words etc. Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</span>
                        </div>
                    </li>
                </ul>

                <div className="chat-message-wrapper">
                    <img src={ellipsisicon} width="20" height="20" alt="Ellipsis Icon" className="chat-other-action"></img>
                    <img src={smileyfaceicon} width="20" height="20" alt="Smiley Face Icon" className="smiley-icon"></img>
                    <img src={attachicon} width="20" height="20" alt="attach Icon" className="attach-icon"></img>
                    <input type="text" name="" id="" placeholder="Type a message"></input>
                    {/* <button type="button" name="" value="" id="" className="add-todo-btn btn btn-primary">Add</button> */}
                </div>

            </div>
        )
    }
}

export default ChatWidget